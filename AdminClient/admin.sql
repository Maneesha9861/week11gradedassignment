create database week11_Admin;
use week11_Admin;

create table admin(
emailid varchar(100) primary key,
adminpassword varchar(100)
);


create table books(
bid int primary key,
bname varchar(100),
bgenre varchar(100),
bprice float,
brating float
);



insert into books value(11,"Wings of Fire","Autobiography",750, 4.8 );
insert into books value(12, "The Alchemist", "Adventure ", 500,4.7 );

insert into books value (13, "Who Will Cry When you Die?", "Personality", 350, 4.6 );

insert into books value (14,"The Power Of Your Subconscious Mind","SelfMotivation",
 430, 4.7);

insert into books value(15,"What-If", "Humor",260,4.5);

select * from  admin;