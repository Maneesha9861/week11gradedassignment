package com.service.test;



import java.util.ArrayList;
import java.util.List;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import com.bean.Books;
import com.dao.BooksDao;
import com.service.BooksService;
@ExtendWith(MockitoExtension.class)
@SpringBootTest
class BooksServiceTest {


	@Mock
	BooksDao booksDao;
	@InjectMocks
	BooksService booksService;
	
	@Test
	void testGetAllBooks() {
		//fail("Not yet implemented");
		List<Books> listBooks= new ArrayList<Books>();
		Books books =new  Books();
		books.setbId(15);
		books.setbName("What-If");
		books.setbGenre("Humor");
		books.setbPrice(260);
		books.setbRating(4.5f);
		listBooks.add(books);
		Mockito.when(booksDao.findAll()).thenReturn(listBooks);
		Assertions.assertTrue(booksService.getAllBooks().stream().count()==1);
	}



	
	@Test
	void testDeleteBook() {
		//fail("Not yet implemented");
		Mockito.when(booksDao.existsById(101)).thenReturn(true);
		Assertions.assertEquals("Book deleted successfully", booksService.deleteBook(101));
		
		
	}

	

	
	

}
