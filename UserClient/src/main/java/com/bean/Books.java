package com.bean;


import javax.persistence.Entity;
import javax.persistence.Id;

//@Entity
public class Books {
	//@Id
	private int bId;
	private String bName;
	private String bGenre;
	private float bPrice;
	private float bRating;


	public Books() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Books(int bId, String bName, String bGenre, float bPrice, float bRating) {
		super();
		this.bId = bId;
		this.bName = bName;
		this.bGenre = bGenre;
		this.bPrice = bPrice;
		this.bRating = bRating;
	}


	public int getbId() {
		return bId;
	}


	public void setbId(int bId) {
		this.bId = bId;
	}


	public String getbName() {
		return bName;
	}


	public void setbName(String bName) {
		this.bName = bName;
	}


	public String getbGenre() {
		return bGenre;
	}


	public void setbGenre(String bGenre) {
		this.bGenre = bGenre;
	}


	public float getbPrice() {
		return bPrice;
	}


	public void setbPrice(float bPrice) {
		this.bPrice = bPrice;
	}


	public float getbRating() {
		return bRating;
	}


	public void setbRating(float bRating) {
		this.bRating = bRating;
	}


	@Override
	public String toString() {
		return "Books [bId=" + bId + ", bName=" + bName + ", bGenre=" + bGenre + ", bPrice=" + bPrice + ", bRating="
				+ bRating + "]";
	}

	
	

}