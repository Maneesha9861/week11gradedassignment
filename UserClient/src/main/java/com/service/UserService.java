package com.service;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.User;
import com.dao.UserDao;

@Service
public class UserService {
	@Autowired
	UserDao userDao;
	
	public String storeUser(User user) {
		if(userDao.existsById(user.getEmailId())) {
			return "Fail User details exist.";
		}
		else {
			userDao.save(user);			
		return "User Stored successfully";
		}
	}
	
	public String login(User user) {
		if(!userDao.existsById(user.getEmailId())) {
			return "User details not presnt ";
		}else {
			User u = userDao.getById(user.getEmailId());
			if(u.getUserPassword().equals(user.getUserPassword())) {
				return "LogIn Successfull";
			}
			else {
				return "LogIn Failed Wrong Password";
			}
		}
	}
	
	public String logOut(String email) {
		if(userDao.existsById(email)) {
		return"LogOut Successfully";
		}
		else {
			return "Invaild User"; 
		}
	}
	


	
	public List<User> getAllUser() {
		return userDao.findAll();
	}


	public String deleteUserInfo(String email) {
		if (!userDao.existsById(email)) {
			return "User   details not present";
		} else {
			userDao.deleteById(email);
			return "User deleted successfully";
		}
	}

	
	

}