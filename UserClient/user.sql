create database week11_User;
use week11_User;

create table User(
emailid varchar (100) primary key,
username varchar(100),
userpassword varchar(100) 
);


create table likedBooks(
emailid varchar (100) ,
bid int,
bname varchar(100),
bgenre varchar(100),
bprice float,
brating float,
primary key(emailid,bid),
foreign key (emailid) references user(emailId)
);
 
 
 create table readLaterBooks(
emailid varchar (100) ,
bid int,
bname varchar(100),
bgenre varchar(100),
bprice float,
brating float,
primary key(emailid, bid),
foreign key (emailid) references user(emailId)
);

 